require 'test_helper'

class TicketitemsControllerTest < ActionController::TestCase
  setup do
    @ticketitem = ticketitems(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ticketitems)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ticketitem" do
    assert_difference('Ticketitem.count') do
      post :create, ticketitem: { integer: @ticketitem.integer, name: @ticketitem.name, name_id: @ticketitem.name_id, price: @ticketitem.price, quantity: @ticketitem.quantity, subtotal: @ticketitem.subtotal, ticket: @ticketitem.ticket, ticket_id: @ticketitem.ticket_id }
    end

    assert_redirected_to ticketitem_path(assigns(:ticketitem))
  end

  test "should show ticketitem" do
    get :show, id: @ticketitem
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ticketitem
    assert_response :success
  end

  test "should update ticketitem" do
    put :update, id: @ticketitem, ticketitem: { integer: @ticketitem.integer, name: @ticketitem.name, name_id: @ticketitem.name_id, price: @ticketitem.price, quantity: @ticketitem.quantity, subtotal: @ticketitem.subtotal, ticket: @ticketitem.ticket, ticket_id: @ticketitem.ticket_id }
    assert_redirected_to ticketitem_path(assigns(:ticketitem))
  end

  test "should destroy ticketitem" do
    assert_difference('Ticketitem.count', -1) do
      delete :destroy, id: @ticketitem
    end

    assert_redirected_to ticketitems_path
  end
end
