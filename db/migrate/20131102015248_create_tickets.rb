class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :name
      t.float :price
      t.integer :quantity
      t.float :subtotal

      t.timestamps
    end
  end
end
