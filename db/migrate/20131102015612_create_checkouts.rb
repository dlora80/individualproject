class CreateCheckouts < ActiveRecord::Migration
  def change
    create_table :checkouts do |t|
      t.integer :ticket_id
      t.float :subtotal
      t.float :tax
      t.float :total
      t.boolean :kitchen_status
      t.boolean :paid_status

      t.timestamps
    end
  end
end
