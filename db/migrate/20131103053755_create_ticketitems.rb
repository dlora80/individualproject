class CreateTicketitems < ActiveRecord::Migration
  def change
    create_table :ticketitems do |t|
      t.integer :ticket_id
      t.string :ticket
      t.string :name_id
      t.string :name
      t.string :integer
      t.float :price
      t.integer :quantity
      t.float :subtotal

      t.timestamps
    end
  end
end
