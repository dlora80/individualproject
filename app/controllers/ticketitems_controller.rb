class TicketitemsController < ApplicationController
  # GET /ticketitems
  # GET /ticketitems.json
  def index
    @ticketitems = Ticketitem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ticketitems }
    end
  end

  # GET /ticketitems/1
  # GET /ticketitems/1.json
  def show
    @ticketitem = Ticketitem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ticketitem }
    end
  end

  # GET /ticketitems/new
  # GET /ticketitems/new.json
  def new
    @ticketitem = Ticketitem.new
    #@ticketitem.ticket_id=params[:ticket]

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ticketitem }
    end
  end

  # GET /ticketitems/1/edit
  def edit
    @ticketitem = Ticketitem.find(params[:id])
  end

  # POST /ticketitems
  # POST /ticketitems.json
  def create
    @ticketitem = Ticketitem.new(params[:ticketitem])

    respond_to do |format|
      if @ticketitem.save
        format.html { redirect_to @ticketitem, notice: 'Ticketitem was successfully created.' }
        format.json { render json: @ticketitem, status: :created, location: @ticketitem }
      else
        format.html { render action: "new" }
        format.json { render json: @ticketitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /ticketitems/1
  # PUT /ticketitems/1.json
  def update
    @ticketitem = Ticketitem.find(params[:id])

    respond_to do |format|
      if @ticketitem.update_attributes(params[:ticketitem])
        format.html { redirect_to @ticketitem, notice: 'Ticketitem was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ticketitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ticketitems/1
  # DELETE /ticketitems/1.json
  def destroy
    @ticketitem = Ticketitem.find(params[:id])
    @ticketitem.destroy

    respond_to do |format|
      format.html { redirect_to ticketitems_url }
      format.json { head :no_content }
    end
  end
end
