class Checkout < ActiveRecord::Base
   attr_accessible :kitchen_status, :paid_status, :subtotal, :tax, :ticket_id, :total

   validates :kitchen_status, :paid_status, :subtotal, :tax, :ticket_id, :total, presence: true

   before_save do
     total
   end

  def total
    self.total = self.subtotal + self.tax
  end

  def tax
    self.tax = self.subtotal * 0.0825
  end
end
