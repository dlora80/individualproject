class Ticketitem < ActiveRecord::Base
  belongs_to :ticket
  belongs_to :menu
  attr_accessible :integer, :name, :name_id, :price, :quantity, :subtotal, :ticket, :ticket_id

  before_save do
    line_total
  end

  def line_total
    self.subtotal= quantity * menu.price
  end
  end
