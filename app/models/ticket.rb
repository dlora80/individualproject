class Ticket < ActiveRecord::Base
  has_many :ticketitems
  has_many :menus, through: :ticketitems
   attr_accessible :name, :price, :quantity, :subtotal

   validates :name, :price, :quantity, presence: true

  before_save do
    subtotal
  end
  def subtotal
  self.subtotal = self.quantity * self.price
    end
end
